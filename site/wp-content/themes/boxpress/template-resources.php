<?php
/**
 * Template Name: Resources
 *
 * Displays the Resources page template
 *
 * @package BoxPress
 */

get_header(); ?>

<?php require_once('template-parts/banners/banner--page.php'); ?>

<div id="primary" class="content-area">
  <main id="main" class="site-main template-resources" role="main">

    <section class="resources-bar">

      <div class="wrap">

        <a href="#blog-articles" class="resource-icon md-newsletter">
          <img src="<?php echo get_template_directory_uri(); ?>/assets/img/global/how-to-article-icon.svg" onerror="this.onerror=null; this.src='<?php echo get_template_directory_uri(); ?>/assets/img/global/how-to-article-icon.png'">
          <p>Newsletter</p>
        </a><!-- end resource-icon blog-articles -->

        <a href="#whitepapers" class="resource-icon md-whitepapers">
          <img src="<?php echo get_template_directory_uri(); ?>/assets/img/global/whitepaper-icon.svg" onerror="this.onerror=null; this.src='<?php echo get_template_directory_uri(); ?>/assets/img/global/whitepaper-icon.png'">
          <p>Whitepapers</p>
        </a><!-- end resource-icon whitepapers -->

        <a href="#whitepapers" class="resource-icon md-whitepapers">
          <img src="<?php echo get_template_directory_uri(); ?>/assets/img/global/whitepaper-icon.svg" onerror="this.onerror=null; this.src='<?php echo get_template_directory_uri(); ?>/assets/img/global/whitepaper-icon.png'">
          <p>Case Studies</p>
        </a><!-- end resource-icon whitepapers -->

        <div class="view-all-container">
          <a href="#view-all" class="view-all">View All</a>
        </div>

      </div><!-- end wrap -->

    </section><!-- end resources-bar -->

    <section class="resources-section">

      <div class="wrap">

        <?php

          $tax = 'resourcetype';
          $query_var = get_query_var( $tax, '' );
          $args = array(
            'post_type'       => 'resources',
            'posts_per_page'  => -1,
            'orderby'         => 'date',
            'order'           => 'DESC',
          );

          if ( ! empty( $query_var )) {
            $args = array_merge( $args, array(
              'tax_query' => array(
                array(
                  'taxonomy' => $tax,
                  'field'    => 'slug',
                  'terms'    => $query_var,
                ),
              ),
            ));
          }

          $the_query = new WP_Query( $args );

        ?>

        <?php if ( $the_query->have_posts() ) : ?>
          <?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>

            <?php
              $terms = get_the_terms( get_the_ID(), $tax );

              $term_slug = array();
              $term_name = array();

              if ( $terms && ! is_wp_error( $terms )) {
                foreach ( $terms as $term ) {
                  $term_slug[] = $term->slug;
                  $term_name[] = $term->name;
                }
              }
            ?>


            <div class="resources-card resources-<?php echo $term_slug[0]; ?>-card">

              <div class="heading resources-<?php echo $term_slug[0]; ?>">

                <img src="<?php echo get_template_directory_uri(); ?>/assets/img/global/<?php echo $term_slug[0]; ?>-icon-small.svg" onerror="this.onerror=null; this.src='<?php echo get_template_directory_uri(); ?>/assets/img/global/<?php echo $term_slug[0]; ?>-icon-small.png'">

                <h2><?php echo $term_name[0]; ?></h2>

                <?php $myTerm = $term_name[0]; ?>

                <!-- <?php echo $myTerm; ?> -->
                <!-- should be below this -->

                <?php if($myTerm == 'Product Videos') { ?>
                  <!-- hello -->
                <?php } ?>

              </div>

              <a href="<?php the_permalink(); ?>">
                <?php
                  if ( has_post_thumbnail() ) {
                    the_post_thumbnail('resources_thumb');
                  } else { ?>
                    <img src="<?php echo get_template_directory_uri(); ?>/assets/img/global/blank.png" />
                  <?php } ?>
              </a>

              <div class="content">

                <a href="<?php the_permalink(); ?>"><h3><?php the_title(); ?></h3></a>

                <p><?php echo excerpt(12); ?></p>

                <?php $myTerm = $term_name[0]; ?>

                <?php if ($myTerm == 'Webinar') { ?>
                  <a class="button" href="<?php the_permalink(); ?>">Watch Now<div class="arrow"></div></a>
                <?php } else { ?>
                  <a class="button" href="<?php the_permalink(); ?>">Read More<div class="arrow"></div></a>
                <?php } ?>

              </div><!-- end content -->

            </div><!-- end resources-card -->

          <?php endwhile; ?>
          <?php wp_reset_postdata(); ?>

        <?php endif; ?>

        <div class="resources-card invisible resources-card-temporary resources-whitepaper-card resources-case-study-card resources-webinar-card resources-capstone-guide-card">
          <p>This helpful area will be available very soon, so be sure to check back here in the future.</p>
          <p>For now, feel free to browse the other categories!</p>
        </div>

      </div><!-- end wrap -->

    </section><!-- end resources-section -->




  </main><!-- #main -->
</div><!-- #primary -->

<?php get_footer(); ?>
