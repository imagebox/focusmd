<?php
/**
 * Template header
 *
 * This is the template that displays all of the <head> section and everything up until <main>
 *
 * @package BoxPress
 */
?>
<!DOCTYPE html>
<html class="no-js" <?php language_attributes(); ?>>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="profile" href="http://gmpg.org/xfn/11">

  <?php wp_head(); ?>

  <?php // Header Tracking Codes ?>
  <?php the_field( 'header_tracking_codes', 'option' ); ?>

</head>
<body <?php body_class(); ?>>

<?php // SVGs ?>
<?php include( get_template_directory() . '/template-parts/global/svg.php'); ?>

<?php // Mobile Header ?>
<?php include( get_template_directory() . '/template-parts/global/header-mobile.php'); ?>

<div class="site-wrap">
<header id="masthead" class="site-header" role="banner">
  <div class="wrap">

    <div class="site-header-inner">
      <div class="header-col-1">

        <div class="site-branding">
          <a href="<?php echo esc_url( home_url( '/' )); ?>" rel="home">
            <img src="<?php bloginfo('template_directory'); ?>/assets/img/branding/nav-logo.png" />
          </a>
        </div>

      </div>
      <div class="header-col-2">

        <?php if ( has_nav_menu( 'primary' )) : ?>

          <nav class="js-accessible-menu navigation--main home-primary-menu"
            aria-label="<?php _e( 'Main Navigation', 'boxpress' ); ?>"
            role="navigation">
            <ul class="nav-list">
                <?php
                  wp_nav_menu( array(
                    'theme_location'  => 'primary',
                    'items_wrap'      => '%3$s',
                    'container'       => false,
                    'walker'          => new Aria_Walker_Nav_Menu(),
                  ));
                ?>
            </ul>
          </nav>

        <?php endif; ?>

      </div>
    </div>

  </div>
</header>

<main id="main" class="site-main" role="main">
