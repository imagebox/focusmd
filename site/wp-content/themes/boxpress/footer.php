<?php
/**
 * Template footer
 *
 * Contains the closing of the main el and all content after.
 *
 * @package BoxPress
 */
?>
</main>
<footer id="colophon" class="site-footer" role="contentinfo">
  <div class="primary-footer">
    <div class="wrap">

      <div class="footer-logo">
        <a href="<?php echo esc_url( home_url( '/' )); ?>" rel="home">
          <img src="<?php bloginfo('template_directory'); ?>/assets/img/branding/footer-logo.png" />
        </a>
      </div>
      <div class="footer-copy">
        <address class="footer-address">
          <?php get_template_part( 'template-parts/global/address-block' ); ?>
        </address>
      </div>


    </div>
  </div>
</footer>
</div>

<?php wp_footer(); ?>

<?php // Footer Tracking Codes ?>
<?php the_field( 'footer_tracking_codes', 'option' ); ?>

</body>
</html>
