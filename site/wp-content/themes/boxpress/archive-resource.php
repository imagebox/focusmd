<?php
/**
 * Template Name: Resources
 *
 * Displays the Resources page template
 *
 * @package BoxPress
 */

$resource_type = get_query_var( 'resource_type' );

get_header(); ?>

  <?php require_once('template-parts/banners/banner--resource.php'); ?>



<section class="filter-bg">
  <?php

    $resource_type_terms = get_terms( array(
      'taxonomy' => 'resource_type',
      'hide_empty' => false,
    ));
  ?>
  <?php if ( $resource_type_terms && ! is_wp_error( $resource_type_terms ) ) : ?>
    <div class="wrap">
      <div class="resource-filter">

          <?php foreach ( $resource_type_terms as $term ) :
              $resource_type_icon = get_field('resource_type_icon', $term);
            ?>

            <div class="filter-content">
<a href="<?php echo esc_url( site_url( '/resources/' ) ); ?>?resource_type=<?php echo $term->slug; ?>"><img src="<?php echo $resource_type_icon;  ?>" alt=""></a>

              <li><a href="<?php echo esc_url( site_url( '/resources/' ) ); ?>?resource_type=<?php echo $term->slug; ?>"><?php echo $term->name; ?></a></li>
            </div>

          <?php endforeach; ?>

          <li>
            <a class="button" href="<?php echo esc_url( site_url( '/resources/' ) ); ?>"><span>View All</span></a>
          </li>

      </div>
    </div>
  <?php endif; ?>
</section>

  <section class="blog-page">
    <div class="wrap">
      <div class="l-grid l-grid--four-col">

          <?php
            $query_resource_args = array(
              'post_type' => 'resource',
              'posts_per_page' => -1,
            );

            if ( ! empty( $resource_type )) {
              $query_resource_args = array_merge_recursive( $query_resource_args, array(
                'tax_query' => array(
              		array(
              			'taxonomy' => 'resource_type',
              			'field'    => 'slug',
              			'terms'    => $resource_type,
              		),
              	),
              ));
            }

            $query_resource = new WP_Query( $query_resource_args );

          ?>

          <?php if ( $query_resource->have_posts() ) : ?>
            <?php while ( $query_resource->have_posts() ) : $query_resource->the_post(); ?>

            <?php
              $resource_type_slug = '';
              $resource_type_name = '';
              $resource_type_icon = '';
              $post_resource_type_terms = get_the_terms( get_the_ID(), 'resource_type' );

              if ( $post_resource_type_terms && ! is_wp_error( $post_resource_type_terms )) {
                foreach ( $post_resource_type_terms as $term ) {
                  $resource_type_slug = $term->slug;
                  $resource_type_name = $term->name;
                  $resource_type_icon = get_field('resource_type_icon', $term);
                }
              }
            ?>


          <div class="l-grid-item">
            <div class="resource-card">

              <div class="resource-card-header     <?php
                      if ( ! empty( $resource_type_slug )) {
                        echo "resource-card--{$resource_type_slug}";
                      }
                    ?>">
                    <div class="resource-title-icon">
                      <?php if ( ! empty( $resource_type_icon ) ) : ?>
                        <img src="<?php echo $resource_type_icon; ?>" alt="">
                      <?php endif; ?>
                      <p><?php echo $resource_type_name; ?></p>
                    </div>
              </div>
              <?php if ( ! empty( $resource_type_name )) : ?>
              <?php endif; ?>
              <div class="resource-card-thumb">
                <?php 	$card_image_thumb = get_field('card_image_thumb'); ?>
                <?php if ( $card_image_thumb ) :
                          $card_image_thumb_url  = $card_image_thumb['sizes'][ 'card_thumb' ];
                          $card_image_thumb_w    = $card_image_thumb['sizes'][ 'card_thumb' . '-width' ];
                          $card_image_thumb_h    = $card_image_thumb['sizes'][ 'card_thumb' . '-height' ];
                        ?>

                                <img class="background-cover-sys" src="<?php echo $card_image_thumb_url; ?>"
                                  width="<?php echo $card_image_thumb_w; ?>"
                                  height="<?php echo $card_image_thumb_h; ?>"
                                  alt="">
                                <?php endif; ?>

                            <!-- thumbnail -->
              </div>
              <div class="resource-card-body">
                <a class="body-title" href="<?php the_permalink(); ?>"><?php the_title(); ?>
                <p><?php the_excerpt(); ?></p>
                <a class="button" href="<?php the_permalink(); ?>"><span><?php
                    if ( $resource_type_slug === 'video' ) {
                      echo 'Watch Now';
                    } else {
                      echo 'Read More';
                    }
                  ?></span></a>
              </div>
            </div>
          </div>

            <?php endwhile; ?>

            <?php wp_reset_postdata(); ?>
          <?php else : ?>

            <?php get_template_part( 'template-parts/content/content', 'none' ); ?>

          <?php endif; ?>
        </div>
      </div>
    </div>
  </div>
  </section>

 <?php get_footer(); ?>
