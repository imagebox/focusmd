(function ($) {
  'use strict';

  /**
   * Site javascripts
   */



  // Wrap video embeds in Flexible Container
  $('iframe[src*="youtube.com"]:not(.not-responsive), iframe[src*="viemo.com"]:not(.not-responsive)').wrap('<div class="flexible-container"></div>');


  var $buttons = $('a.button, a.button-2, ul.button li');

  $buttons.each(function () {
    var $this = $(this);

    if ( ! $this.has( 'span' ).length ) {
      $this.wrapInner( '<span></span>' );
    }
  });

  var $owl_carousel = $('.js-owl-carousel');

  $owl_carousel.each(function () {
    var $this = $(this);
    var carousel_loop = false;
    var carousel_nav  = false;
    var carousel_dots = false;
    var mouse_drag    = false;
    var touch_drag    = false;

    if ( $this.find( '> div' ).length > 1 ) {
      carousel_loop = true;
      carousel_nav  = true;
      carousel_dots = true;
      mouse_drag    = true;
      touch_drag    = true;
    }

    // Carousels
    $this.owlCarousel({
      items: 1,
      mouseDrag: mouse_drag,
      touchDrag: touch_drag,
      dots: carousel_dots,
      nav: carousel_nav,
      loop: carousel_loop,
      navSpeed: 500,
      dotsSpeed: 500,
      dragEndSpeed: 500,
      autoplay:true,
      autoplayHoverPause: true,
      navText: [
        '<svg class="svg-left-icon" width="16" height="19"><use xlink:href="#arrow-left-icon"></use></svg>',
        '<svg class="svg-right-icon" width="16" height="19"><use xlink:href="#arrow-right-icon"></use></svg>',
      ]
    });
  });

})(jQuery);
