<?php
/**
 * Template Name: Homepage
 *
 * Page template to display the homepage.
 *
 * @package BoxPress
 */
get_header(); ?>


<?php

$card_section_header = get_field('card_section_header');
$card_section_link = get_field('card_section_link');
$card_section_link_mobile = get_field('card_section_link_mobile');
$card_header_a = get_field('card_header_a');
$card_header_b = get_field('card_header_b');
$card_header_c = get_field('card_header_c');
$card_copy_a = get_field('card_copy_a');
$card_copy_b = get_field('card_copy_b');
$card_copy_c = get_field('card_copy_c');
$card_link_a = get_field('card_link_a');
$card_link_b = get_field('card_link_b');
$card_link_c = get_field('card_link_c');
$card_icon_a = get_field('card_icon_a');
$card_icon_b = get_field('card_icon_b');
$card_icon_c = get_field('card_icon_c');
$our_client_header = get_field('our_client_header');
$our_client_copy = get_field('our_client_copy');
$our_client_link = get_field('our_client_link');
$our_client_image = get_field('our_client_image');
$term_id = 5;
$post_icon = get_field('category_image', 'category_'. $term_id);
$resource_section_header = get_field('resource_section_header');
$resource_section_link = get_field('resource_section_link');
$request_demo_section_header = get_field('request_demo_section_header');


 ?>


  <article class="homepage">

    <?php get_template_part( 'template-parts/homepage/homepage-hero' ); ?>
    <!-- Hero  -->


      <section class="section learn-more-section">
        <div class="wrap">
          <div class="learn-more-header">
            <h2><?php echo $card_section_header; ?></h2>
            <?php if( $card_section_link ): ?>

            <a class="button" href="<?php echo $card_section_link['url']; ?>" target="<?php echo $card_section_link['target']; ?>"><?php echo $card_section_link['title']; ?></a>

            <a class="mobile-link-hide" href="<?php echo $card_section_link_mobile_mobile['url']; ?>" target="<?php echo $card_section_link_mobile['target']; ?>"><?php echo $card_section_link_mobile['title']; ?></a>

            <?php endif; ?>
          </div>
          <div class="learn-more-body">
            <div class="learn-more-card">
              <div class="learn-more-card-header">
                <h4><?php echo $card_header_a; ?></h4>
                <?php
                  if( !empty($card_icon_a) ): ?>

                  	<img src="<?php echo $card_icon_a['url']; ?>" alt="<?php echo $card_icon_a['alt']; ?>" />

                  <?php endif; ?>
              </div>
              <p><?php echo $card_copy_a; ?></p>
              <?php
                if( $card_link_a ): ?>

              	<a class="button invert-button" href="<?php echo $card_link_a['url']; ?>" target="<?php echo $card_link_a['target']; ?>"><?php echo $card_link_a['title']; ?></a>

                <?php endif; ?>
             </div>
            <div class="learn-more-card">
              <div class="learn-more-card-header">
                <h4><?php echo $card_header_b; ?></h4>
                <?php
                  if( !empty($card_icon_b) ): ?>

                    <img src="<?php echo $card_icon_b['url']; ?>" alt="<?php echo $card_icon_b['alt']; ?>" />

                  <?php endif; ?>
              </div>
              <p><?php echo $card_copy_b; ?></p>
              <?php
                if( $card_link_b ): ?>

              	<a class="button invert-button" href="<?php echo $card_link_b['url']; ?>" target="<?php echo $card_link_b['target']; ?>"><?php echo $card_link_b['title']; ?></a>

                <?php endif; ?>
             </div>
            <div class="learn-more-card">
              <div class="learn-more-card-header">
                <h4><?php echo $card_header_c; ?></h4>
                <?php
                  if( !empty($card_icon_c) ): ?>

                    <img src="<?php echo $card_icon_c['url']; ?>" alt="<?php echo $card_icon_c['alt']; ?>" />

                  <?php endif; ?>
              </div>
              <p><?php echo $card_copy_c; ?></p>
              <?php
                if( $card_link_c ): ?>

              	<a class="button invert-button" href="<?php echo $card_link_c['url']; ?>" target="<?php echo $card_link_c['target']; ?>"><?php echo $card_link_c['title']; ?></a>

                <?php endif; ?>
             </div>
          </div>
        </div>
      </section>
    <!-- Solution section  -->

    <section class="section our-clients-section">
      <div class="wrap">
        <div class="our-client-content">
          <div class="our-client-image">
            <div class="our-client-image-box">
              <?php if ( $our_client_image ) :
                $image_size 	= 'block_image_width';
                $image_url 		= $our_client_image['sizes'][ $image_size ];
                $image_width 	= $our_client_image['sizes'][ $image_size . '-width' ];
                $image_height =  $our_client_image['sizes'][ $image_size . '-height' ];
                $image_alt 		=  $our_client_image['alt'];
                ?>

                <div class="split-image">
                  <img src="<?php echo $image_url; ?>"
                  width="<?php echo $image_width; ?>"
                  height="<?php echo $image_height; ?>"
                  alt="<?php echo $image_alt; ?>">
                </div>

              <?php endif; ?>
            </div>
          </div>

          <div class="our-client-body">
            <div class="our-client-header">
              <h2><?php echo $our_client_header; ?></h2>
            </div>
            <div class="our-client-copy">
              <p><?php echo $our_client_copy; ?></p>

              <?php if( $our_client_link ): ?>

                <a class="button" href="<?php echo $our_client_link['url']; ?>" target="<?php echo $our_client_link['target']; ?>"><?php echo $our_client_link['title']; ?></a>

              <?php endif; ?>
            </div>
          </div>
        </div>
      </div>
    </section>
    <!-- Our client section  -->

  <section class="section request-demo-section">
    <div class="wrap">
      <div class="request-demo-content">
        <div class="request-demo-body">
          <div class="request-demo-header">
            <h2><?php echo $request_demo_section_header; ?></h2>
          </div>
          <div class="request-demo-copy">
            <address class="footer-address">
              <?php get_template_part( 'template-parts/global/address-block' ); ?>
            </address>
          </div>
        </div>
        <div class="request-demo-form">
          <?php gravity_form( 1, true, false, false, '', false ); ?>
        </div>
      </div>
    </div>
  </section>
  <!-- Our client section  -->

  </article>

<?php get_footer(); ?>
