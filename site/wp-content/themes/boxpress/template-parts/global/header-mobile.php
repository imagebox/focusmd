
<div class="site-header--mobile">
  <div class="mobile-header-left">
    <div class="site-branding">
      <a href="<?php echo esc_url( home_url( '/' )); ?>" rel="home">
        <img src="<?php bloginfo('template_directory'); ?>/assets/img/branding/nav-logo-mobile.png" />
      </a>
    </div>
  </div>
  <div class="mobile-header-right">
    <a href="#" class="menu-button toggle-nav">
      <span class="vh"><?php _e('Menu', 'boxpress'); ?></span>
      <svg class="menu-icon-svg" width="48" height="33">
        <use xlink:href="#menu-icon"></use>
      </svg>
    </a>
  </div>
</div>

<div class="mobile-nav-tray">
  <nav class="navigation--mobile">
    <div class="mobile-nav-header">
      <a href="<?php echo esc_url( home_url( '/' )); ?>" rel="home">
        <img src="<?php bloginfo('template_directory'); ?>/assets/img/branding/nav-logo-mobile.png" />
      </a>
      <a class="toggle-nav" href="#">
        <span class="vh"><?php _e('Close', 'boxpress'); ?></span>
        <svg class="menu-icon-svg" width="25" height="26">
          <use xlink:href="#close-icon"></use>
        </svg>
      </a>
    </div>

    <?php if ( has_nav_menu( 'primary' ) ) : ?>
      <ul class="mobile-nav mobile-nav--main">
        <?php
          wp_nav_menu( array(
            'theme_location'  => 'primary',
            'items_wrap'      => '%3$s',
            'container'       => false,
          ));
        ?>
      </ul>
    <?php endif; ?>
  </nav>
</div>
