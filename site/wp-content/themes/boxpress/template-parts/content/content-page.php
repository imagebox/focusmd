<?php
/**
 * Template part for displaying page content in page.php
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package BoxPress
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class( 'content--page' ); ?>>

  <header class="page-header">

  </header>

  <div class="page-content">
    <?php the_content(); ?>
  </div>

</article>
