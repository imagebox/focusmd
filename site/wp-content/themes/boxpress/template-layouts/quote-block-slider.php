<?php
/**
 * Displays the Parallax Background layout
 *
 * @package BoxPress
 */




?>


<section class="slider-container">
    <div class="js-owl-carousel owl-carousel owl-theme ">

        <?php if ( have_rows( 'slider_quote_repeater' )) : ?>

         <?php while ( have_rows( 'slider_quote_repeater' )) : the_row();
               $slider_quote_bkg        = get_sub_field( 'slider_quote_background' );
               $slider_quote_bkg_image  = get_sub_field( 'slider_quote_background_image' );
               $slider_quote_bkg_size   = 'block_full_width';
               $slider_quote_name =  get_sub_field( 'slider_quote_name' );
               $slider_quote_title =  get_sub_field( 'slider_quote_title' );
               $slider_quote =  get_sub_field( 'slider_quote' );
               $slider_quote_bkg_size  = 'block_full_width';

           ?>

             <div class="quote-block-layout section <?php echo $slider_quote_bkg; ?>">

               <div class="wrap wrap--limited">

                 <blockquote class="quote-block">
                   <div class="quote-block-body">
                  <p><?php echo $slider_quote; ?></p>
                     <h2><?php echo $quote_header; ?></h2>
                   </div>

                   <?php if ( ! empty( $slider_quote_name ) ) : ?>

                     <cite class="quote-block-citation">
                       <span class="quote-name"><?php echo $slider_quote_name; ?></span>
                       <?php if ( ! empty( $slider_quote_title )) : ?>
                         <span class="quote-title"><?php echo $slider_quote_title; ?></span>
                       <?php endif; ?>
                     </cite>

                   <?php endif; ?>

                 </blockquote>

               </div>
              <div class="focusmd-overlay">
                <?php if ( $slider_quote_bkg_image && $slider_quote_bkg === 'background-image' ) : ?>
                  <img class="quote-block-layout-bkg" draggable="false"
                    src="<?php echo esc_url( $slider_quote_bkg_image['sizes'][ $slider_quote_bkg_size ] ); ?>"
                    width="<?php echo esc_attr( $slider_quote_bkg_image['sizes'][ $slider_quote_bkg_size . '-width' ] ); ?>"
                    height="<?php echo esc_attr( $slider_quote_bkg_image['sizes'][ $slider_quote_bkg_size . '-height' ] ); ?>"
                    alt="">
                <?php endif; ?>
              </div>

             </div>
             <?php endwhile; ?>
           <?php endif; ?>
          <!-- slides-->

       </div>
