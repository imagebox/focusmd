<?php
/**
 * The template for displaying archive pages.
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package BoxPress
 */

get_header(); ?>

  <?php require_once('template-parts/banners/banner--blog.php'); ?>

  <section class="blog-page">
    <div class="wrap">

      <div class="l-sidebar">
        <div class="l-main-col">

          <?php if ( have_posts() ) : ?>

            <header class="page-header">
              <h1 class="page-title"><?php echo post_type_archive_title( '', false ); ?></h1>
            </header>

            <?php while ( have_posts() ) : the_post(); ?>

              <?php get_template_part( 'template-parts/content/content', get_post_format() ); ?>

            <?php endwhile; ?>
            <?php the_posts_navigation(); ?>
          <?php else : ?>

            <?php get_template_part( 'template-parts/content/content', 'none' ); ?>

          <?php endif; ?>

        </div>
        <div class="l-aside-col">

          <?php get_sidebar(); ?>

        </div>
      </div>
    </div>
  </section>

<?php get_footer(); ?>
