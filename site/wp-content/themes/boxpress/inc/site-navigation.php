<?php
/**
 * Register site navigations
 *
 * @package BoxPress
 */

function boxpress_register_theme_navs() {
  register_nav_menus( array(
    'primary'    => __( 'Primary Menu', 'boxpress' ),
    'secondary'    => __( 'Secondary Menu', 'boxpress' ),
    'button'     => __( 'Button Menu', 'boxpress' ),
  ));
}
add_action( 'after_setup_theme', 'boxpress_register_theme_navs' );
