<?php
/**
 * The template for displaying all single posts.
 *
 * @package BoxPress
 */

$resource_type_slug = '';
$post_resource_type_terms = get_the_terms( get_the_ID(), 'resource_type' );

if ( $post_resource_type_terms && ! is_wp_error( $post_resource_type_terms )) {
 foreach ( $post_resource_type_terms as $term ) {
   $resource_type_slug = $term->slug;
 }
}

get_header(); ?>

  <?php require_once('template-parts/banners/banner--resource.php'); ?>

  <section class="blog-page">
    <div class="wrap wrap--limited">

      <?php while ( have_posts() ) : the_post(); ?>

        <?php get_template_part( 'template-parts/content/content', 'single' ); ?>

      <?php endwhile; ?>

    </div>

    <?php

      $query_related_resource_args = array(
        'post_type' => 'resource',
        'posts_per_page' => 3,
        'post__not_in' => array( get_the_ID() ),
        'tax_query' => array(
          array(
            'taxonomy' => 'resource_type',
            'field'    => 'slug',
            'terms'    => $resource_type_slug,
          ),
        ),
      );

      $query_related_resource = new WP_Query( $query_related_resource_args );
    ?>


    <div class="wrap wrap--limited">
      <h4>Related Posts</h4>
      <div class="l-grid l-grid--four-col single-resource">
    <?php if ( $query_related_resource->have_posts() ) : ?>
      <?php while ( $query_related_resource->have_posts() ) : $query_related_resource->the_post(); ?>
          <div class="l-grid-item">
          <a href="<?php the_permalink(); ?>">
          <!-- title -->
          <div class="resource-card-thumb">
          <?php 	$card_image_thumb = get_field('card_image_thumb'); ?>
                <?php if ( $card_image_thumb ) :
                    $card_image_thumb_url  = $card_image_thumb['sizes'][ 'card_thumb' ];
                    $card_image_thumb_w    = $card_image_thumb['sizes'][ 'card_thumb' . '-width' ];
                    $card_image_thumb_h    = $card_image_thumb['sizes'][ 'card_thumb' . '-height' ];
                ?>

                <img class="background-cover-sys" src="<?php echo $card_image_thumb_url; ?>"
                  width="<?php echo $card_image_thumb_w; ?>"
                  height="<?php echo $card_image_thumb_h; ?>"
                  alt="">
                <?php endif; ?>
              <!-- thumbnail -->
              <!-- date -->
            </a>
          </div>
          <a href="<?php the_permalink(); ?>"><p class="related-post-title"><?php the_title(); ?></p></a>
          <p class="similiar-date"><?php echo get_the_date('m.j.Y');?></p>
          <p class="similar-post">Similar Post</p>
        </div>
      <?php endwhile; ?>

      <?php wp_reset_postdata(); ?>
    <?php endif; ?>
  </section>

<?php get_footer(); ?>
