// Utils
var fs      = require('fs');
var log     = require('fancy-log');
var chalk   = require('chalk');

// Gulps
var gulp    = require('gulp');
var watch   = require('gulp-watch');
var runSequence = require('run-sequence');
var cat     = require('gulp-cat');
var concat  = require('gulp-concat');
var plumber = require('gulp-plumber');
var inject  = require('gulp-inject');
var rename  = require('gulp-rename');
var clone   = require('gulp-clone');

// JS
var uglify  = require('gulp-uglify');

// SASS
var sass      = require('gulp-sass');
var postcss   = require('gulp-postcss');
var sassGlob  = require('gulp-sass-glob');

// CSS
var cssnano         = require('cssnano');
var autoprefixer    = require('autoprefixer');
var mqpacker        = require('css-mqpacker');
var discardComments = require('postcss-discard-comments');

// SVG
var svgstore = require('gulp-svgstore');
var svgmin   = require('gulp-svgmin');

// BrowserSync
var browserSync = require('browser-sync').create();

// Packaging Theme
var zip = require('gulp-zip');

// Configuration
var config    = require('./package.json').gulp;
var themePath = './site/wp-content/themes/' + config.theme;
var assetPath = themePath + '/assets';



/**
 * Serve
 */

gulp.task('browser-sync', function () {

  /**
   * BrowserSync
   */

  browserSync.init({
    proxy: 'http://localhost:8080'
  });
});



/**
 * SASS
 * ----
 */

gulp.task('sass', function () {

  var processors = [
    autoprefixer({
      browsers: ['last 3 versions']
    }),
    mqpacker({
      sort: true
    }),
    discardComments()
  ];

  return gulp.src('./site/wp-content/themes/boxpress/assets/scss/*.scss')
    .pipe(plumber(function (error) {
      log.error( error.message );
      this.emit('end');
    }))
    .pipe(sassGlob())
    .pipe(sass({
      includePaths: [
        './node_modules/breakpoint-sass/stylesheets'
      ],
      errLogToConsole: true,
      outputStyle: 'nested',
      precision: 12
    }))
    .pipe(postcss(processors))
    .pipe(gulp.dest('./site/wp-content/themes/boxpress/assets/css'));
});


/**
 * CSS
 * ---
 * Creates min file while preserving the original
 */

gulp.task('css:minify', function () {
  
  var processors = [
    cssnano({
      discardComments: {
        removeAll: true
      }
    })
  ];

  return gulp.src([
    './site/wp-content/themes/boxpress/assets/css/*.css',
    '!./site/wp-content/themes/boxpress/assets/css/*.min.css'
  ])
    .pipe(clone())
    .pipe(rename(function (path) {
      path.basename += '.min';
      path.extname = '.css';
    }))
    .pipe(postcss(processors))
    .pipe(gulp.dest('./site/wp-content/themes/boxpress/assets/css/'))
    .pipe(browserSync.stream());
});



/**
 * SVG
 * ---
 * [https://github.com/w0rm/gulp-svgstore]
 * [https://www.npmjs.com/package/gulp-svgmin]
 *
 * Combines and Inlines SVGs below the opening body tag
 */

gulp.task('svg', function () {
  var svgs = gulp.src('./site/wp-content/themes/boxpress/assets/svg/**/*.svg')
    .pipe( svgmin( function (file) {
      return {
        plugins: [{
          removeViewBox: false
        }, {
          cleanupIDs: {
            minify: true
          }
        }]
      };
    }))
    .pipe( svgstore({ inlineSvg: true }));

  function fileContents(filePath, file) {
    return file.contents.toString();
  }

  return gulp.src([
    './site/wp-content/themes/boxpress/template-parts/global/svg.php'
  ])
    .pipe(inject(svgs, { transform: fileContents }))
    .pipe(gulp.dest('./site/wp-content/themes/boxpress/template-parts/global/'));
});



/**
 * JS
 * --
 */

gulp.task('js', function () {
  return gulp.src([
    './site/wp-content/themes/boxpress/assets/js/libs/*.js',
    './site/wp-content/themes/boxpress/assets/js/plugins/*.js',
    './site/wp-content/themes/boxpress/assets/js/*.js'
  ])
    .pipe(plumber())
    .pipe(concat('site.js'))
    .pipe(gulp.dest('./site/wp-content/themes/boxpress/assets/js/build'));
});

gulp.task('js:minify', function () {
  return gulp.src([
    './site/wp-content/themes/boxpress/assets/js/build/*.js',
    '!./site/wp-content/themes/boxpress/assets/js/build/*.min.js',
  ])
    .pipe(clone())
    .pipe(uglify())
    .pipe(rename(function (path) {
      path.basename += '.min';
      path.extname = '.js';
    }))
    .pipe(gulp.dest( './site/wp-content/themes/boxpress/assets/js/build/' ))
    .pipe(browserSync.stream());
});



/**
 * Zip
 * ---
 */

gulp.task('package', function () {
  return gulp.src([
    './site/wp-content/themes/boxpress/**/*',
    '!./site/wp-content/themes/boxpress/sftp-config.json'
  ])
    .pipe(zip('boxpress.zip'))
    .pipe(gulp.dest('dist'));
});



/**
 * Gulp
 * ----
 */

// Watch
gulp.task( 'watch', function () {

  watch([
    './site/wp-content/themes/boxpress/assets/js/**/*.js',
    '!./site/wp-content/themes/boxpress/assets/js/build/**/*.js',
  ]).on('change', function () {
    runSequence( 'js', 'js:minify' );
  });

  watch([
    './site/wp-content/themes/boxpress/assets/scss/**/*.scss'
  ]).on('change', function () {
    runSequence( 'sass', 'css:minify' );
  });

  watch([
    './site/wp-content/themes/boxpress/assets/svg/**/*.svg'
  ]).on('change', function () {
    runSequence( 'svg' );
  });

});

// Default Task
gulp.task('default', [ 'watch' ], function () {
  return gulp.src('./BOXPRESS.md')
    .pipe(cat());
});

gulp.task('serve', [ 'browser-sync', 'watch' ], function () {
  return gulp.src('./BOXPRESS.md')
    .pipe(cat());
});
